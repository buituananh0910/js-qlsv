function layThongTinTuForm() {
  const maSV = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matkhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;
  return new SinhVien(tenSV, maSV, matkhau, email, diemToan, diemLy, diemHoa);
}

function renderDSSV(svArr) {
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    var trContent = `<tr>
    <td>${sv.ma}</td> 
    <td>${sv.ten}</td> 
    <td>${sv.email}</td> 
    <td>${sv.tinhDTB()}</td> 
    <td>
    <button class="btn btn-danger" onclick="xoaSinhVien('${
      sv.ma
    }')">Xoá</button>
    <button class="btn btn-warning" onclick="suaSinhVien('${
      sv.ma
    }')">Sửa</botton> 
    </td>
    </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      return index;
    }
  }
  return -1;
}
function xoaSinhVien(id) {
  console.log(id);
  // var index = dssv.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  var index = timKiemViTri(id, dssv);
  console.log("idex:", index);
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
  }
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matkhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("id: ", id);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
    document.getElementById("txtMaSV").disabled = true;
  }
}
