const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";
var dssv = [];
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matkhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDSSV(dssv);
}
function themSV() {
  var newSv = layThongTinTuForm();
  if (kiemTraFull(newSv)) {
    dssv.push(newSv);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    console.log("dssv: ", dssv);
  }
}

function capNhatSV() {
  var oldSV = layThongTinTuForm();
  var index = timKiemViTri(oldSV.ma, dssv);
  if (kiemTraFull(oldSV)) {
    dssv[index] = oldSV;
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
  }
}

function resetSV() {
  // localStorage.clear();
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtMaSV").disabled = false;
}

function searchSV() {
  var tenSV = document.getElementById("txtSearch").value;
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.ten == tenSV) {
      var trContent = `<tr>
    <td>${sv.ma}</td> 
    <td>${sv.ten}</td> 
    <td>${sv.email}</td> 
    <td>${sv.tinhDTB()}</td> 
    <td>
    <button class="btn btn-danger" onclick="xoaSinhVien('${
      sv.ma
    }')">Xoá</button>
    <button class="btn btn-warning" onclick="suaSinhVien('${
      sv.ma
    }')">Sửa</botton> 
    </td>
    </tr>`;
      document.getElementById("tbodySinhVien").innerHTML = trContent;
    }
  }
}

function kiemTraFull(newSv) {
  var isValid =
    validator.kiemTraRong(newSv.ma, "spanMaSV") &&
    validator.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      4,
      4,
      "Mã sinh viên phải có 4 kí tự"
    );
  isValid &=
    validator.kiemTraRong(newSv.ten, "spanTenSV") &&
    validator.kiemTraDoDai(
      newSv.ten,
      "spanTenSV",
      0,
      16,
      "Tên sinh viên không dài hơn 16 kí tự"
    );
  isValid &=
    validator.kiemTraRong(newSv.email, "spanEmailSV") &&
    validator.kiemTraEmail(newSv.email, "spanEmailSV", "Email không hợp lệ");
  // validator.kiemTraRong(newSv.matkhau, "spanMatKhau") &
  // validator.kiemTraRong(newSv.toan, "spanToan") &
  // validator.kiemTraRong(newSv.ly, "spanLy") &
  // validator.kiemTraRong(newSv.hoa, "spanHoa");
  return isValid;
}
